﻿namespace web;

public interface IRelatorio
{
    Task Imprimir(HttpContext context);
}