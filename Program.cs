using web;

var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();

//app.MapGet("/", () => "Ol� Mundo!");

//app.Run();

var serviceCollection = new ServiceCollection();
serviceCollection.AddTransient<ICatalogo, Catalogo>();
serviceCollection.AddTransient<IRelatorio, Relatorio>();

var serviceProvider = serviceCollection.BuildServiceProvider();

ICatalogo catalogo = serviceProvider.GetService<ICatalogo>();
IRelatorio relatorio = serviceProvider.GetService<IRelatorio>();
//IRelatorio relatorio = new Relatorio(catalogo);

app.MapGet("/", async (context) =>
{
  await relatorio.Imprimir(context);

});
    
app.Run();

    